import java.util.Random;

public class Monster implements Entity {
    private String name;
    private int health;
    private int stamina;
    private int strength;

    private final String[] enemyList = {
        "Rato Infectado",
        "Peste Viva",
        "Humano Enlouquecido",
        "Cão Derretido"
    };

    Monster() {
        Random random = new Random();
        this.name = enemyList[random.nextInt(enemyList.length)];
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public int getStamina() {
        return stamina;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void heal(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        this.health += amount;
    }

    public void takeDamage(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        if (this.health - amount < 0) {
            this.health = 0;
        } else {
            this.health -= amount;
        }
    }

    public void attack(Entity e, int amount) {
        e.takeDamage(amount);
        this.stamina -= 1;
    }
}

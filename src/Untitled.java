public class Untitled {
    public static void main(String[] args) {
        // 0 - barreira
        // 1 - caminho
        // 2 - começo
        int[][] map = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 2, 0, 0, 8, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 4, 1, 7, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 9, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 1, 3, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        Game game = new Game(new State(map));

        game.run();
    }
}

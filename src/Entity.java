public interface Entity {
    public void heal(int amount);
    public void takeDamage(int amount);
    public void attack(Entity e, int amount);
}

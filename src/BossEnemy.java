import java.util.Random;

public class BossEnemy extends Enemy {
    private int lifePoints;
    private int staminaPoints;
    private double actionPoints;
    // Configurando uma lista de nomes para serem aleatoriamente gerados
    private final String[] bossList = {"Aberração de Mercúrio", "Emusk, o Explorador", "Ronald McRony", "Sr. Reaganol"};
    Random random = new Random();

    //Gerando nome aleatório
    int bossIndex = random.nextInt(bossList.length);
    String bossGenerated = bossList[bossIndex];


    @Override
    public void setLifepoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    @Override
    public int getLifepoints() {
        return this.lifePoints;
    }

    @Override
    public void setStaminapoints(int staminaPoints) {
        this.staminaPoints = staminaPoints;
    }

    @Override
    public int getStaminapoints() {
        return this.staminaPoints;
    }

    @Override
    public void setActionpoints(double actionPoints) {
        this.actionPoints = actionPoints;
    }

    @Override
    public double getActionpoints() {
        return this.actionPoints;
    }

    public String getBossgenerated() {
        return this.bossGenerated;
    }
}

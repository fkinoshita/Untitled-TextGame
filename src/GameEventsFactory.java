import java.util.Scanner;

public class Game {
     private Player player = new Player();
     private Enemy enemy = new Enemy();
     private BossEnemy boss = new BossEnemy();
     private Scanner userInput = new Scanner(System.in);

    // metodo de criação do jogador
    public void createPlayer() {
        // construindo nome
        System.out.println("Olá, bem vindo ao Untitled!\nDigite seu nome jovem aventureiro: ");
        this.player.setPlayerName(userInput.next());
        System.out.println("- Este é um jogo estilo Text-Adventure onde você, " + player.getPlayername() + ", descobre os segredos da metrópole Toxicity!\n- Uma metrópole que outrora vivia seu tempo de ouro, e agora afunda em químicos tóxicos,\n- produzidos pela insaciável fome por capital da burguesia.");

        // criando atributos essenciais
        player.setLifepoints(50);
        player.setStaminapoints(30);
        player.setActionpoints(3.0);
    }

public void createEnemy() {

        switch(enemy.enemyIndex) {

            // Rato Infectado
            case 1:
                enemy.setLifepoints(10);
                enemy.setStaminapoints(10);
                enemy.setActionpoints(3.0);
                break;

            // Peste Viva
            case 2:
                enemy.setLifepoints(20);
                enemy.setStaminapoints(20);
                enemy.setActionpoints(3.0);
                break;

            // Humano Enlouquecido
            case 3:
                enemy.setLifepoints(40);
                enemy.setStaminapoints(25);
                enemy.setActionpoints(3.0);
                break;
            // Cão Derretido
            case 4:
                enemy.setLifepoints(30);
                enemy.setStaminapoints(30);
                enemy.setActionpoints(2.0);
                break;
        }
    }

    public void createBoss() {

        switch(boss.bossIndex) {

            // Aberração de Mercúrio
            case 1:
                boss.setLifepoints(100);
                boss.setStaminapoints(20);
                boss.setActionpoints(2.0);
                break;

            // Emusk, o Explorador
            case 2:
                boss.setLifepoints(60);
                boss.setStaminapoints(30);
                boss.setActionpoints(5.0);
                break;

            // Ronald McRony
            case 3:
                boss.setLifepoints(50);
                boss.setStaminapoints(50);
                boss.setActionpoints(3.0);
                break;
            // Sr. Reaganol
            case 4:
                boss.setLifepoints(60);
                boss.setStaminapoints(30);
                boss.setActionpoints(4.0);
                break;
        }
    }

    public void printStatus() {
        System.out.println("Você está com: " + player.getLifepoints() + " de Vida");
        System.out.println("Você possui: " + player.getStaminapoints() + " de Energia");
        System.out.println("Você possui: " + player.getActionpoints() + " Pontos de Ação a serem utilizados");
    }
}

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Game {
    private State state;

    Game (State initialState) {
        this.state = initialState;
    }

    public void run() {
        introduction();
        String play = getUserInput("S/N? ");

        switch (play.toLowerCase()) {
        case "n":
            say("É uma pena que pense assim, precisamos da sua ajuda!");
            System.exit(0);
        }

        createCharacter();

        say("Você está pronto para sua primeira missão.");

        this.state.getPlayer().setPosX(2);
        this.state.getPlayer().setPosY(1);

        boolean running = true;
        while (running) {
            currentMessage();
            String response = getUserInput("> ");
            handleInput(response);

            say(String.format("POS (%d, %d)", this.state.getPlayer().getPosX(), this.state.getPlayer().getPosY()));
        }
    }

    private void introduction() {
        String story =
            """
            Você é um dos únicos sobreviventes que pode nos ajudar.
            Já fazem 27 anos desde que esses monstros nos invadiram
            e acabaram com nosso planeta.
            Por favor, chegue até o antigo quartel do exército
            e acione o botão de emergência.
            Irá nos ajudar?
            """;
        System.out.println(story);
    }

    private void currentMessage() {
        int X = this.state.getPlayer().getPosX();
        int Y = this.state.getPlayer().getPosY();

        int currentTile = this.state.mapTileAt(X, Y);

        showPossiblePaths();

        switch (currentTile) {
        case 3:
            say("Você encontrou o antigo quartel do exército, conseguimos vencer os monstros!");
            System.exit(0);
        case 4:
            // this.enterBattle();
            break;
        case 7:
            say("You found a chest buried in the ground.\nWhat will you do?");
            break;
        case 8:
            say("You found a key hidden beneath a rock.\nWhat will you do?");
            break;
        case 9:
            say("You're facing an old wooden door.\nWhat will you do?");
            break;
        }
    }

    private void handleInput(String input) {
        int X = this.state.getPlayer().getPosX();
        int Y = this.state.getPlayer().getPosY();

        switch (input) {
        case "sair":
            System.exit(0);
        case "ir norte":
            if (this.state.isMapTileWalkable(X, Y - 1)) {
                this.state.getPlayer().walkNorth();
            }
            break;
        case "ir sul":
            if (this.state.isMapTileWalkable(X, Y + 1)) {
                this.state.getPlayer().walkSouth();
            }
            break;
        case "ir leste":
            if (this.state.isMapTileWalkable(X + 1, Y)) {
                this.state.getPlayer().walkEast();
            }
            break;
        case "ir oeste":
            if (this.state.isMapTileWalkable(X - 1, Y)) {
                this.state.getPlayer().walkWest();
            }
            break;
        default:
            System.out.println("Eu não sei o que isso significa.");
            break;
        }
    }

    private void createCharacter() {
        System.out.println("Escolha o nome do seu personagem:");

        String characterName = getUserInput("> ");
        System.out.println();

        this.state.setPlayer(new Player(characterName, getCharacterClass()));
    }

    private static int getCharacterClass() {
        System.out.println("Escolha a classe do seu personagem:");
        System.out.println("  1. Guerreiro");
        System.out.println("  2. Mago");
        System.out.println("  3. Artilheiro");

        String characterClass = getUserInput("> ");
        characterClass = characterClass.toLowerCase();
        System.out.println();

        int choice = 1;

        switch (characterClass) {
        case "1":
        case "guerreiro":
            System.out.println("Parabéns, você é um Guerreiro!");
            choice = 1;
            break;
        case "2":
        case "mago":
            System.out.println("Parabéns, você é um Mago!");
            choice = 2;
            break;
        case "3":
        case "artilheiro":
            System.out.println("Parabéns, você é um Artilheiro!");
            choice = 3;
            break;
        default:
            return getCharacterClass();
        }

        return choice;
    }

    private void showPossiblePaths() {
        int X = this.state.getPlayer().getPosX();
        int Y = this.state.getPlayer().getPosY();

        int up = this.state.mapTileAt(X, Y - 1);
        int down = this.state.mapTileAt(X, Y + 1);
        int left = this.state.mapTileAt(X - 1, Y);
        int right = this.state.mapTileAt(X + 1, Y);

        int[] paths = {up, down, left, right};

        String possible = new String();
        for (int i = 0; i < paths.length; i++) {
            possible += String.format("%d ", paths[i] != 0 ? 1 : 0);
        }
        possible = possible.trim();

        switch (possible) {
            //N S W E
        case "0 0 0 0":
            say("Sem caminho adiante.");
            break;
        case "0 0 0 1":
            say("YVocê só pode ir para o leste.");
            break;
        case "0 0 1 0":
            say("YVocê só pode ir para o oeste.");
            break;
        case "0 0 1 1":
            say("Você pode ir para o oeste ou leste.");
            break;
        case "0 1 0 0":
            say("Você só pode ir para o sul.");
            break;
        case "0 1 0 1":
            say("Você pode ir para o sul ou leste.");
            break;
        case "0 1 1 0":
            say("Você pode ir para o sul ou oeste.");
            break;
        case "0 1 1 1":
            say("Você pode ir para o sul, oeste, ou leste.");
            break;
        case "1 0 0 0":
            say("Você só pode ir para o norte.");
            break;
        case "1 0 0 1":
            say("Você pode ir para o norte ou leste.");
            break;
        case "1 0 1 0":
            say("Você pode ir para o norte ou oeste.");
            break;
        case "1 0 1 1":
            say("Você pode ir para o norte, oeste, ou leste.");
            break;
        case "1 1 0 0":
            say("Você pode ir para o norte ou sul.");
            break;
        case "1 1 0 1":
            say("Você pode ir para o norte, sul, ou leste.");
            break;
        case "1 1 1 0":
            say("Você pode ir para o norte, sul, ou oeste.");
            break;
        case "1 1 1 1":
            say("Você pode ir para o norte, sul, oeste, ou leste.");
            break;
        }
    }

    private static void say(String output) {
        System.out.println(output + "\n");
    }

    private static String getUserInput(String prompt) {
        String input = new String();

        System.out.print(prompt);

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            input = reader.readLine();
        } catch (Exception e) {
            System.out.println("An error occured: " + e.toString());
        }

        return input;
    }
}

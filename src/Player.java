public class Player implements Entity {
    private String name;
    private int posX;
    private int posY;
    private int health;
    private int stamina;
    private double strength;

    Player(String name, int characterClass) {
        this.name = name;
        switch (characterClass) {
        case 1:
            // warrior
            break;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int X) {
        this.posX = X;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int Y) {
        this.posY = Y;
    }

    public void walkNorth() {
        this.posY -= 1;
    }

    public void walkSouth() {
        this.posY += 1;
    }

    public void walkWest() {
        this.posX -= 1;
    }

    public void walkEast() {
        this.posX += 1;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public double getStrength() {
        return strength;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    public void heal(int amount) {
        if (amount < 0) {
            // Lançando uma exceção ao receber argumentos não desejados.
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        this.health += amount;
    }

    public void takeDamage(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative.");
        }

        if (this.health - amount < 0) {
            this.health = 0;
        } else {
            this.health -= amount;
        }
    }

    public void attack(Entity e, int amount) {
        e.takeDamage(amount);
        this.stamina -= 1;
    }
}

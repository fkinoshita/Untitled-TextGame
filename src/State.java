public class State {
    private Player player;
    private int[][] map;

    State(int[][] map) {
        this.map = map;
    }

    public void setMap(int[][] map) {
        this.map = map;
    }

    public int[][] getMap() {
        return this.map;
    }

    public int mapTileAt(int X, int Y) {
        return map[Y][X];
    }

    public boolean isMapTileWalkable(int X, int Y) {
        return map[Y][X] != 0;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}
